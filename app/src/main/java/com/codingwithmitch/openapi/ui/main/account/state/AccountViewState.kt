package com.codingwithmitch.openapi.ui.main.account.state

import com.codingwithmitch.openapi.models.AccountProperties

data class AccountViewState(
    var accountProperties: AccountProperties? = null
) {
}

