package com.codingwithmitch.openapi.persistence

import androidx.room.Database
import androidx.room.RoomDatabase
import com.codingwithmitch.openapi.models.AccountProperties
import com.codingwithmitch.openapi.models.AuthToken
import com.codingwithmitch.openapi.models.BlogPost

@Database(entities = [AccountProperties::class, AuthToken::class, BlogPost::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getAccountDao(): AccountDao
    abstract fun getAuthDao(): AuthTokenDao
    abstract fun getBlogPostDao(): BlogPostDao

    companion object {
        val DATABASE_NAME = "app_database"
    }

}